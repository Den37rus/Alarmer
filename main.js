const {
  app,
  ipcMain,
  BrowserWindow,
  Tray,
  Menu,
  nativeImage,
} = require('electron')

const { join } = require('path')

let mainWindow, settingsWindow

function createSetingsWindow() {
  settingsWindow = new BrowserWindow({
    width: 580,
    height: 680,
    skipTaskbar: true,
    resizable: false,
    frame: false,
    webPreferences: {
      spellcheck: false,
      preload: join(__dirname, 'settings.js'),
      contextIsolation: true,
    },
  })

  settingsWindow.loadFile('settings.html')
}

function createWindow() {
  mainWindow = new BrowserWindow({
    fullscreen: true,
    width: 700,
    height: 500,
    skipTaskbar: true,
    resizable: false,
    alwaysOnTop: true,
    webPreferences: {
      preload: join(__dirname, 'requests.js'),
      contextIsolation: true,
    },
    frame: false,
  })
  mainWindow.loadFile('index.html')

  const iconpath = join(__dirname, 'alarm.png')
  const trayIcon = nativeImage.createFromPath(iconpath)
  const appIcon = new Tray(trayIcon)
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Открыть',
      click: function () {
        mainWindow.show()
      },
    },
    {
      label: 'Установки',
      click: function () {
        if (settingsWindow) {
          settingsWindow.show()
        } else {
          createSetingsWindow()
        }
      },
    },
    {
      label: 'Выход',
      click: function () {
        app.isQuiting = true
        app.quit()
        process.exit(0)
      },
    },
  ])
  appIcon.setContextMenu(contextMenu)
  appIcon.setToolTip('Отзвоны')
  appIcon.on('click', () => {
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show()
  })

  mainWindow.on('close', function (event) {
    event.preventDefault()
    mainWindow.hide()
    return false
  })

  mainWindow.on('minimize', function (event) {
    event.preventDefault()
    mainWindow.hide()
  })

  mainWindow.on('show', function () {
    appIcon.setHighlightMode
  })
}

app.on('ready', () => {
  createWindow()
  mainWindow.hide()
})

ipcMain.on('alarm', () => {
  mainWindow.show()
})

ipcMain.on('closeSettings', () => {
  mainWindow.webContents.send('chageSettings')
  settingsWindow.close()
  settingsWindow = null
})

ipcMain.handle('chageSettings', () => {
  mainWindow.webContents.send('chageSettings')
})
