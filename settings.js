const { ipcRenderer, webFrame } = require('electron')
const { $, Time, DayChecker, BIN_SVG } = require('./support')

webFrame.setZoomLevel(0.5)

class Alert {
  constructor(data = {}) {
    this.id = Date.now() + Math.trunc(Math.random() * 1000)
    this.daily = data.daily || new Array(7).fill(true)
    this.time =
      data.time ||
      new Date()
        .toLocaleTimeString('ru-RU', {
          hour: '2-digit',
          minute: '2-digit',
        })
        .split(':')
        .map((el) => +el)

    this.status = data.status ?? true
    this.message = data.message || ''
  }

  setHours(val) {
    this.time[0] = val
  }

  setMinutes(val) {
    this.time[1] = val
  }

  changeMessage(mess) {
    this.message = mess
  }

  changeStatus() {
    this.status = !this.status
  }

  changeDays(day) {
    this.daily[day] = !this.daily[day]
  }
}

class AlertsSettings {
  constructor() {
    this.alerts =
      JSON.parse(localStorage.getItem('alerts'))?.map((el) => new Alert(el)) ||
      []
  }

  clear() {
    this.alerts = []
    this.save()
  }

  remove(id) {
    this.alerts.splice(
      this.alerts.findIndex((el) => el.id == id),
      1
    )
    return this
  }

  create() {
    const l = new Alert()
    this.alerts.push(l)
    this.save()
    return l
  }

  save() {
    return new Promise((resolve, reject) => {
      try {
        localStorage.setItem('alerts', JSON.stringify(this.alerts))
        resolve(this.alerts)
      } catch (e) {
        reject(e.message)
      }
    })
  }
}

class AlertElement {
  constructor(alertSettings) {
    if (!(alertSettings instanceof Alert)) {
      alert('error in alerts store,clear local storage')
    }
    this.template = `
      <div class="alert-item">
        <div class="alert-time">
          <div class="time alert-item__hours"></div>
            <div class="time" style="width: 10px;margin-top: -2px;">:</div>
            <div class="time alert-item__minutes"></div>
        </div>
        <ul class="alert-item__daily"></ul>
        <div class="alert-switch">
        <div class="onoffswitch">
            <input type="checkbox" id="switch-${alertSettings.id}" class="onoffswitch-checkbox">
            <label class="onoffswitch-label" for="switch-${alertSettings.id}"></label>
          </div>
        </div>
        <div class="bin">${BIN_SVG}</div>
      </div>
      <div class="alert-message">
        <div>Alarm message: </div>
        <input class="alert-message__text"></input>
      </div>
    `

    this.rootElement = document.createElement('div')
    this.rootElement.className = 'alert-item-wraper'
    this.rootElement.innerHTML = this.template

    const hoursEl = this.rootElement.querySelector('.alert-item__hours')
    const hours = new Time(hoursEl, 24, alertSettings.time[0])
    hoursEl.addEventListener('wheel', (e) => {
      alertSettings.setHours(hours.scroll(e))
    })

    const minutesEl = this.rootElement.querySelector('.alert-item__minutes')
    const minutes = new Time(minutesEl, 60, alertSettings.time[1])
    minutesEl.addEventListener('wheel', (e) => {
      alertSettings.setMinutes(minutes.scroll(e))
    })

    const messageEl = this.rootElement.querySelector('.alert-message__text')
    messageEl.value = alertSettings.message
    messageEl.addEventListener('input', (e) => {
      alertSettings.changeMessage(e.target.value)
    })

    const switchEl = this.rootElement.querySelector('.onoffswitch-checkbox')
    switchEl.checked = alertSettings.status
    switchEl.addEventListener('change', () => {
      alertSettings.changeStatus()
    })

    const daily = this.rootElement.querySelector('.alert-item__daily')
    new DayChecker(daily, alertSettings)

    const binEl = this.rootElement.querySelector('.bin')
    binEl.addEventListener('click', (e) => {
      settings
        .remove(alertSettings.id)
        .save()
        .then((v) => {
          this.hideElememt()
        })
    })

    this.rootElement.addEventListener('mouseleave', (e) => {
      settings.save()
    })
  }

  get html() {
    return this.rootElement
  }

  hideElememt() {
    this.rootElement.classList.remove('show-alert')
  }

  showElement() {
    this.rootElement.classList.add('show-alert')
  }
}

const creatAlertElement = (rootEl, alarm) => {
  const al = new AlertElement(alarm)
  rootEl.append(al.html)
  setTimeout(() => {
    al.showElement()
  }, 0)
}

const settings = new AlertsSettings()

window.addEventListener('load', () => {
  const container = $('.alerts')
  settings.alerts.forEach((a) => {
    creatAlertElement(container, a)
  })

  const closeBtn = $('.close-img')
  closeBtn.$el.addEventListener('click', () => {
    settings.save().then(() => {
      ipcRenderer
        .invoke('chageSettings')
        .then(() => ipcRenderer.send('closeSettings'))
    })
  })

  const addBtn = $('.addAlertBtn')
  addBtn.$el.addEventListener('click', () => {
    creatAlertElement(container, settings.create())
  })

  const titleTimeNode = document.querySelector('.curr-time')
  titleTimeNode.textContent = new Date().toLocaleTimeString('ru')
  setInterval(() => {
    titleTimeNode.textContent = new Date().toLocaleTimeString('ru')
  }, 1000)
})
