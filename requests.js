const { ipcRenderer } = require('electron')

const getDateTime = () => {
  const date = new Date()
  return {
    day: date.getDay(),
    time: [date.getHours(), date.getMinutes()],
    seconds: date.getSeconds(),
  }
}

const setCurrentDayAlerts = () => {
  const alertsArray = JSON.parse(localStorage.getItem('alerts'))
  if (alertsArray) {
    return alertsArray.filter((i) => {
      if (i.daily[getDateTime().day] && i.status) return i.time
    })
  }
  return []
}

const timer = (indicatorFn, showMessFn) => {
  let dayAlerts = setCurrentDayAlerts()
  ipcRenderer.on('chageSettings', () => {
    dayAlerts = setCurrentDayAlerts()
  })
  setInterval(() => {
    const dateTime = getDateTime()
    indicatorFn(dateTime)
    if (dateTime.time.every((item) => item == 0)) {
      dayAlerts = setCurrentDayAlerts()
    }
    dayAlerts.forEach((arr, index) => {
      if (arr.time[0] == dateTime.time[0] && arr.time[1] == dateTime.time[1]) {
        ipcRenderer.send('alarm')
        showMessFn(arr)
        dayAlerts.splice(index, 1)
      }
    })
  }, 1000)
}

window.addEventListener('DOMContentLoaded', () => {
  const $clock = document.querySelector('#clock')
  const $sentryText = document.querySelector('#sentry')

  const normalizeDateTime = (currentDate) => {
    const { day, time, seconds } = currentDate
    const [hours, minutes] = time.map((el) => (el < 10 ? `0${el}` : `${el}`))
    const secondsNormalized = seconds < 10 ? `0${seconds}` : seconds
    const normalizedTime =
      `${hours} : ${minutes}` +
      (seconds !== undefined ? `: ${secondsNormalized}` : '')
    const days = [
      'Воскресенье',
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота',
    ]
    return {
      day: days[day],
      fullTime: normalizedTime,
    }
  }

  const showClock = (data) => {
    const normalaizedDate = normalizeDateTime(data)
    $clock.innerHTML = `${normalaizedDate.day} <br/> ${normalaizedDate.fullTime}`
  }

  const showAlarmText = (alarmData) => {
    $sentryText.classList.add('d-animation')
    const time = normalizeDateTime({ time: alarmData.time })
    $sentryText.innerHTML = `${alarmData.message}<br/> ${
      alarmData.message ? 'в' : ''
    } ${time.fullTime}`
  }

  const clearSentryAnimation = () => {
    $sentryText.innerHTML = ''
    $sentryText.classList.remove('d-animation')
  }

  timer(showClock, showAlarmText)

  document.querySelectorAll('.btn-close').forEach(($el) => {
    $el.addEventListener('click', () => {
      clearSentryAnimation()
      window.close()
    })
  })
})
